#!/usr/bin/env python

EXPANSIONS = ['VRCVI', 'VRCVII', 'FDS Sound', 'MMC5 Audio', 'Namco 106', 'Sunsoft FME-7']

def parse(data):
    region = ord(data[0x7a])
    chips = ord(data[0x7b])
    expansions = []
    i = 0
    while chips:
        if chips & 1:
            expansions.append(EXPANSIONS[i])
        chips >>= 1
        i += 1
    return {
        'version': ord(data[0x05]),
        'songs': ord(data[0x06]),
        'name': data[0x0e:0x2e].split('\0', 1)[0].decode('iso-8859-1').encode('utf8'),
        'artist': data[0x2e:0x4e].split('\0', 1)[0].decode('iso-8859-1').encode('utf8'),
        'copyright': data[0x4e:0x6e].split('\0', 1)[0].decode('iso-8859-1').encode('utf8'),
        'region': 'PAL/NTSC' if region & 2 else 'NTSC' if not region else 'PAL',
        'expansions': expansions
    }

def fileparse(filename):
    data = file(filename).read(0x80)
    return parse(data)

if __name__ == '__main__':
    import sys, json
    print json.dumps(fileparse(sys.argv[1]))
