#!/usr/bin/env python
import cStringIO

EXPANSIONS = ['VRCVI', 'VRCVII', 'FDS Sound', 'MMC5 Audio', 'Namco 106', 'Sunsoft FME-7']

def __readn(f, n):
    v = 0
    data = f.read(n)[::-1]
    for c in data:
        v <<= 8
        v |= ord(c)
    return v

def __stri(s):
    v = 0
    s = s[::-1]
    for c in s:
        v <<= 8
        v |= ord(c)
    return v

def __readblock(f):
    blockname = f.read(16).rstrip('\0')
    version = __readn(f, 4)
    length = __readn(f, 4)
    return blockname, version, f.read(length)

def __countsongs(version, f, channels):
    count = 0
    while True:
        frames = f.read(4)
        if len(frames) < 4:
            break
        frames = __stri(frames)
        if version == 1:
            channels = __readn(f, 4)
        if version == 2:
            __readn(f, 4) # tempo
            __readn(f, 4) # rows per frame
        if version >= 3:
            __readn(f, 4) # speed
            __readn(f, 4) # tempo
            __readn(f, 4) # rows per frame

        length = channels * frames
        f.read(length)
        count += 1

    return count


def parse(f):
    f.read(len('FamiTracker Module'))
    identifier = __readn(f, 4)
    blocks = {}
    while 'PARAMS' not in blocks or 'INFO' not in blocks or 'FRAMES' not in blocks:
        name, version, data = __readblock(f)
        if not data:
            return {}
        blocks[name] = (version, data)

    expansions = []
    if blocks['PARAMS'][0] >= 2:
        chips = ord(blocks['PARAMS'][1][0])
        i = 0
        while chips:
            if chips & 1:
                expansions.append(EXPANSIONS[i])
            chips >>= 1
            i += 1

    if blocks['PARAMS'][0] == 1:
        channels = __stri(blocks['PARAMS'][1][4:8])
        pal = __stri(blocks['PARAMS'][1][8:12])
    else:
        channels = __stri(blocks['PARAMS'][1][1:5])
        pal = __stri(blocks['PARAMS'][1][5:9])

    f_version = blocks['FRAMES'][0]

    songs = __countsongs(f_version, cStringIO.StringIO(blocks['FRAMES'][1]), channels)

    return {
        'version': identifier,
        'name': blocks['INFO'][1][0x00:0x20].split('\0', 1)[0].decode('iso-8859-1').encode('utf8'),
        'artist': blocks['INFO'][1][0x20:0x40].split('\0', 1)[0].decode('iso-8859-1').encode('utf8'),
        'copyright': blocks['INFO'][1][0x40:0x60].split('\0', 1)[0].decode('iso-8859-1').encode('utf8'),
        'expansions': expansions,
        'songs': songs,
        'region': 'PAL' if pal else 'NTSC'
    }

    return info

def fileparse(filename):
    return parse(file(filename))

if __name__ == '__main__':
    import sys, json
    print json.dumps(fileparse(sys.argv[1]))
